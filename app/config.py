from os import path

PROGRAM_VERSION = '1.1.3'
BASEDIR = path.abspath(path.dirname(__file__))
UPLOAD_FOLDER = path.join(BASEDIR, 'samples')
BACKGROUND_NAMES = ['BGN', 'Blank', 'Blanc', 'BNG', 'blank', 'blanc']
BGN = 'BGN'
SRC = 'SRC'
C_NAMES = ['A', 'B', 'C', 'AB', 'BC', 'AC', 'D', 'T']
COINC_NAMES = ['AB', 'BC', 'AC', 'D', 'T']
PKG_NAME = ['N1', 'M1', 'N2', 'M2']

DB_FILE = path.join(BASEDIR, 'data', 'nuclides.csv')
TEMP_DB = path.join(BASEDIR, 'data', 'tmp_nucl.dat')
STOPPING_POWER_DB = path.join(BASEDIR, 'data', 'dedxTanXia.txt')

COCKTAIL = {
    'UltimaGold': 1,
    'UltimaGold XR': 2,
    'UltimaGold AB': 3,
    'UltimaGold LLT': 4,
    'Insta-Gel Plus': 5,
    'Hionic-Flour': 6,
    'OptiFlour': 7,
    'Optiphase Hisafe 2': 8,
    'Optiphase Hisafe 3': 9,
    'UltimaGold MV': 10,
    'UltimaGold F': 11,
    'Toluene PPO 5g/l': 12,
}

THEME = 'Dark'

AVAILABLE_NUCLIDES = [
    'H-3',
    'C-14',
    'Ni-63',
    'Tc-99',
    'Pm-147',
    'Sr-90',
    'Y-90',
    'Sr-89',
    'Ca-45',
    'Cl-36',
    'S-35',
    'P-33',
    'P-32',
]


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
