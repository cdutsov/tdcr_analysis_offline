from os import path
from datetime import datetime
import tkinter as tk
from tkinter import filedialog
from numpy import arange
from app import runtime_config, io, config, analyze, colors


class VerticalScrolledFrame(tk.Frame):
    """A pure Tkinter scrollable frame that actually works!
    * Use the 'interior' attribute to place widgets inside the scrollable frame
    * Construct and pack/place/grid normally
    * This frame only allows vertical scrolling
    """
    def __init__(self, parent, *args, **kw):
        tk.Frame.__init__(self, parent, *args, **kw)

        # create a canvas object and a vertical scrollbar for scrolling it
        vscrollbar = tk.Scrollbar(self, orient=tk.VERTICAL)
        vscrollbar.pack(fill=tk.Y, side=tk.RIGHT, expand=tk.FALSE)
        self.canvas = tk.Canvas(self,
                                bd=0,
                                highlightthickness=0,
                                yscrollcommand=vscrollbar.set)
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.TRUE)
        vscrollbar.config(command=self.canvas.yview)

        # reset the view
        self.canvas.xview_moveto(0)
        self.canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = tk.Frame(self.canvas)
        self.interior_id = self.canvas.create_window(0,
                                                     0,
                                                     window=interior,
                                                     anchor=tk.NW)
        self.interior.bind('<Configure>', self._configure_interior)
        self.canvas.bind('<Configure>', self._configure_canvas)
        self.interior.bind_all("<Button-4>", self._on_mousewheel_up)
        self.interior.bind_all("<Button-5>", self._on_mousewheel_down)

    # track changes to the canvas and frame width and sync them,
    # also updating the scrollbar
    def _configure_interior(self, event):
        # update the scrollbars to match the size of the inner frame
        size = (self.interior.winfo_reqwidth(),
                self.interior.winfo_reqheight())
        self.canvas.config(scrollregion="0 0 %s %s" % size)
        if self.interior.winfo_reqwidth() != self.canvas.winfo_width():
            # update the canvas's width to fit the inner frame
            self.canvas.config(width=self.interior.winfo_reqwidth())

    def _configure_canvas(self, event):
        if self.interior.winfo_reqwidth() != self.canvas.winfo_width():
            # update the inner frame's width to fill the canvas
            self.canvas.itemconfigure(self.interior_id,
                                      width=self.canvas.winfo_width())

    def _on_mousewheel_up(self, event):
        self.canvas.yview_scroll(int(-1), "units")

    def _on_mousewheel_down(self, event):
        self.canvas.yview_scroll(int(1), "units")


class Gui:
    def __init__(self):
        ''' Main window '''
        self.manual_meas_list = {}
        self.manual_fields = {}
        self.measurements = {}

        self.root = tk.Tk()
        self.root.minsize(1200, 800)
        self.scrollframe = VerticalScrolledFrame(self.root)
        self.scrollframe.pack(expand=True, fill='both')

        self.leftframe = tk.Frame(self.scrollframe.interior, width=600, bd=20)
        self.leftframe.columnconfigure(0, weight=1)
        self.leftframe.rowconfigure(0, weight=1)
        self.rightframe = tk.LabelFrame(self.scrollframe,
                                        width=600,
                                        text='Results')
        self.root.columnconfigure(0, weight=2)

        self.auto_frame = tk.LabelFrame(
            self.leftframe,
            width=800,
            text='Auto analyze files from folder',
        )

        self.manual_frame = tk.LabelFrame(
            self.leftframe, width=800, text='Add measurements for calculation')

        self.param_frame = tk.LabelFrame(
            self.leftframe,
            text='Select parameters and caclulate activity',
            width=800,
        )

        self.kb_frame = tk.Frame(self.param_frame, bd=10)
        self.cock_frame = tk.Frame(self.param_frame, bd=10)
        self.date_frame = tk.Frame(self.param_frame, bd=10)
        self.pack_frame = tk.Frame(self.param_frame, bd=10)
        self.skip_frame = tk.Frame(self.param_frame, bd=10)
        self.acc_frame = tk.Frame(self.param_frame, bd=10)

        message = "Selected working directory: None"
        self.work_dir_label = tk.Label(self.auto_frame,
                                       text=message,
                                       bg=colors.bgn)
        self.work_dir_label.grid(column=0,
                                 columnspan=2,
                                 row=1,
                                 padx=65,
                                 pady=10,
                                 sticky='nwe')

        # self.leftframe.grid(column=0, row=0, sticky='nwe')
        # self.leftframe.grid_columnconfigure(0, weight=1)
        self.leftframe.pack(side=tk.LEFT, fill='both', expand=True)
        # self.rightframe.grid(column=1, row=0, sticky='nwe', padx=30, pady=40)
        self.rightframe.pack(side=tk.RIGHT, fill='both', expand=True)

        self.auto_frame.grid(padx=30, pady=20)
        self.manual_frame.grid(padx=0, pady=20)
        self.param_frame.grid(padx=10, pady=20)

        self.date_frame.grid(sticky='nwe')
        self.cock_frame.grid(sticky='nwe')
        self.kb_frame.grid(sticky='nwe')
        self.pack_frame.grid(sticky='nwe')
        self.skip_frame.grid(sticky='nwe')
        self.acc_frame.grid(sticky='nwe')

        self.textbox = tk.Text(self.rightframe, width=90)
        self.textbox.pack(expand=True, fill='both', padx=10, pady=10)
        self.textbox.pack_propagate(False)

        self.ip = io.InputParameters()
        self.kb_str = tk.StringVar()
        self.kb_str.set('0.01')
        self.kb_entry = tk.Entry(self.kb_frame,
                                 highlightthickness=3,
                                 validate='key',
                                 textvariable=self.kb_str)
        self.kb_str.trace_add('write', self.select_kb_callback)
        # self.kb_entry.configure(
        #     validatecommand=(self.select_kb_callback, '%S'))
        self.cock_choice = tk.StringVar()
        self.cock_choice.set('Select Cocktail')
        self.cock_choice.trace_add('write', self.update_cock)
        self.ref_date_str = tk.StringVar()
        self.ref_date_str.set(datetime.now().strftime('%Y-%m-%d %H:%M'))
        self.pkg_choice = tk.StringVar()
        self.pkg_choice.set('N1')
        self.skip_first = tk.BooleanVar()
        self.correct_acc = tk.BooleanVar()

        self.root.title("TDCR analysis tool v." + config.PROGRAM_VERSION)

        self.meas_field_add()
        self.select_work_dir()
        self.auto_analyze_init()
        self.select_kb()
        self.select_cocktail()
        self.select_ref_date()
        self.select_packet()
        self.select_skip()
        self.select_acc()
        self.calculate()

        self.set_colors(self.root)
        self.root.mainloop()

    def select_work_dir(self):
        select_dir_btn = tk.Button(self.auto_frame,
                                   text='Select Directory',
                                   command=self.select_work_dir_callback)
        select_dir_btn.grid(column=0, row=0, pady=10)

    def meas_field_add(self):
        add_meas_btn = tk.Button(self.manual_frame,
                                 text='Manually add measurement +',
                                 command=self.meas_field)
        add_meas_btn.grid(column=0, row=1, columnspan=2, pady=20, padx=60)

    def meas_field(self):
        pos = 9 + len(self.measurements)
        # number = kwargs['number']
        number = len(self.measurements)
        select_src_btn = tk.Button(self.manual_frame,
                                   text='Select Source',
                                   command=lambda: self.append_meas(
                                       number=number, meas_type=config.SRC))
        field_id = config.SRC + str(number)
        self.manual_fields.update({field_id: select_src_btn})
        select_src_btn.grid(column=0, row=pos, padx=5, pady=5)
        select_bgn_btn = tk.Button(self.manual_frame,
                                   text='Select Blank',
                                   command=lambda: self.append_meas(
                                       number=number, meas_type=config.BGN))
        field_id = config.BGN + str(number)
        self.manual_fields.update({field_id: select_bgn_btn})
        select_bgn_btn.grid(column=1, row=pos, padx=5, pady=5)

        self.set_colors(self.manual_frame)

    def append_meas(self, **kwargs):
        number = kwargs['number']
        meas_type = kwargs['meas_type']
        select_file = tk.filedialog.askopenfilename(defaultextension='.xml',
                                                    filetypes=[
                                                        ("nanoTDCR files",
                                                         ".xml")
                                                    ])
        if select_file:
            meas = io.import_file(select_file)
            if meas_type == config.SRC:
                self.measurements.update({number: meas})
            else:
                self.measurements[number].bgn_guess = meas
            field_id = meas_type + str(number)
            fname = path.basename(select_file)
            self.manual_fields[field_id].configure(text=fname,
                                                   bg=colors.green,
                                                   fg=colors.bgn)

    def auto_analyze_init(self):
        auto_analyze_btn = tk.Button(self.auto_frame,
                                     text='Auto Analyze Folder',
                                     command=self.auto_analyze_callback)
        auto_analyze_btn.grid(column=1, row=0)

    def auto_analyze_callback(self):
        measurements = analyze.auto_analyze()
        for meas in measurements:
            bgn_fname = meas.bgn_guess.basename
            i = len(self.measurements)
            self.meas_field()
            self.manual_fields[config.SRC +
                               str(i)].configure(text=meas.basename)
            self.manual_fields[config.BGN + str(i)].configure(text=bgn_fname)
            self.measurements.update({i: meas})

    def auto_manual_combine(self):
        for _num, manual in self.manual_meas_list.items():
            src = io.import_file(manual[config.SRC])
            bgn = io.import_file(manual[config.BGN])
            src.bgn_guess = bgn
            self.measurements.update({manual[config.SRC]: src})

    def select_work_dir_callback(self):
        selection = filedialog.askdirectory()
        if selection:
            runtime_config.WORK_DIR = selection
            message = "Working dir selected\n" +\
                runtime_config.WORK_DIR
            self.work_dir_label.configure(text=message)
        # self.measurements.update(analyze.auto_analyze())

    def select_kb(self):
        message = "Value of kB (cm/MeV):"
        kb_label = tk.Label(self.kb_frame, text=message)
        kb_label.pack(side=tk.LEFT)
        self.kb_entry.pack(side=tk.RIGHT)

    def select_kb_callback(self, a, b, c):
        kbs_str = self.kb_str.get()
        try:
            ranges = [float(r) for r in kbs_str.split(':')]
            if len(ranges) == 3:
                kbs = arange(*ranges)
            elif len(ranges) == 2:
                kbs = arange(*ranges, 0.001)
            else:
                kbs = ranges
            self.update_kb(kbs)
        except BaseException:
            self.kb_entry.configure(highlightcolor=colors.red)
            self.kb_entry.configure(highlightbackground=colors.red)
            return True
        return True

    def update_kb(self, kbs):
        if 0 < len(kbs) < 20:
            runtime_config.KBS = kbs
            self.kb_entry.configure(highlightcolor=colors.green)
            self.kb_entry.configure(highlightbackground=colors.green)
            return True
        self.kb_entry.configure(highlightcolor=colors.red)
        self.kb_entry.configure(highlightbackground=colors.red)
        return True

    def select_cocktail(self):
        message = "Select cocktail:"
        choices = config.COCKTAIL.keys()
        cock_label = tk.Label(self.cock_frame, text=message)
        cock_label.pack(side=tk.LEFT)
        cock_list = tk.OptionMenu(self.cock_frame, self.cock_choice, *choices)
        cock_list.pack(side=tk.RIGHT)

    def update_cock(self, *_args):
        cock = config.COCKTAIL[self.cock_choice.get()]
        runtime_config.COCKTAIL_NUMBER = cock
        runtime_config.COCKTAIL_NAME = self.cock_choice.get()
        return True

    def select_packet(self):
        message = "Select packet:"
        choices = config.PKG_NAME
        pkg_label = tk.Label(self.pack_frame, text=message)
        pkg_label.pack(side=tk.LEFT)
        pkg_list = tk.OptionMenu(self.pack_frame, self.pkg_choice, *choices)
        pkg_list.pack(side=tk.RIGHT)

    def update_pkg(self, *_args):
        runtime_config.PACKET_NAME = self.pkg_choice.get()
        return True

    def select_ref_date(self):
        message = "Select reference date:"
        date_label = tk.Label(self.date_frame, text=message)
        date_label.pack(side=tk.LEFT)
        self.date_entry = tk.Entry(self.date_frame,
                                   highlightthickness=3,
                                   validate="all",
                                   textvariable=self.ref_date_str)
        self.ref_date_str.trace_add('write', self.update_ref_date)
        self.date_entry.pack(side=tk.RIGHT)

    def update_ref_date(self, *_args):
        try:
            ref_date = datetime.strptime(self.ref_date_str.get(),
                                         '%Y-%m-%d %H:%M')
        except ValueError:
            try:
                ref_date = datetime.strptime(self.ref_date_str.get(),
                                             '%Y-%m-%d %H:%M:%S')
            except ValueError:
                try:
                    ref_date = datetime.strptime(self.ref_date_str.get(),
                                                 '%Y-%m-%d')
                except ValueError:
                    self.date_entry.configure(highlightbackground=colors.red)
                    self.date_entry.configure(highlightcolor=colors.red)
                    return True

        runtime_config.REFERENCE_DATE = ref_date
        self.date_entry.configure(highlightbackground=colors.green)
        self.date_entry.configure(highlightcolor=colors.green)
        return True

    def select_skip(self):
        label = "Skip first run of measurement?"
        skip_label = tk.Label(self.skip_frame, text=label)
        skip_label.pack(side=tk.LEFT)
        skip_entry = tk.Checkbutton(self.skip_frame, variable=self.skip_first)
        skip_entry.pack(side=tk.RIGHT)

    def update_skip(self):
        runtime_config.SKIP_FIRST = self.skip_first.get()

    def select_acc(self):
        label = "Correct for accidental coincidences?"
        acc_label = tk.Label(self.acc_frame, text=label)
        acc_label.pack(side=tk.LEFT)
        acc_entry = tk.Checkbutton(self.acc_frame, variable=self.correct_acc)
        acc_entry.pack(side=tk.RIGHT)

    def update_acc(self):
        runtime_config.CORRECT_ACC = self.correct_acc.get()

    def calculate(self):
        calc_btn = tk.Button(self.leftframe,
                             text='Calculate',
                             command=self.calculate_callback)
        calc_btn.grid()

    def calculate_callback(self):
        self.auto_manual_combine()
        self.update_ref_date()
        self.update_cock()
        self.select_kb_callback(None, None, None)
        self.update_pkg()
        self.update_acc()
        self.update_skip()
        text = tk.StringVar()
        results = analyze.calculate_all(self.measurements.values())
        text_str = ''.join([str(r) for r in results])
        text.set(text_str)
        self.textbox.delete('1.0', tk.END)
        self.textbox.insert(tk.INSERT, text.get())

    def set_colors(self, master):
        widgets = []
        widgets = self.all_children(master, widgets)
        widgets.append(master)
        for widget in widgets:
            self.try_change_color(widget, bg=colors.bgn)
            self.try_change_color(widget, fg=colors.white)
            self.try_change_color(widget, activebackground=colors.cyan)
            self.try_change_color(widget, activeforeground=colors.bgn)
            self.try_change_color(widget, highlightbackground=colors.cyan)
            self.try_change_color(widget, highlightcolor=colors.cyan)
            self.try_change_color(widget, disabledforeground=colors.cyan)
            self.try_change_color(widget, selectcolor=colors.bgn)
            self.try_change_color(widget, font=('Mono', 10))
        # self.try_change_color(self.param_frame, borderwidth=5)

    def try_change_color(self, widget, **kwargs):
        try:
            widget.configure(**kwargs)
        except BaseException:
            pass

    def all_children(self, wid, fin_list):
        _list = wid.winfo_children()
        for item in _list:
            fin_list.append(item)
            self.all_children(item, fin_list)
        return fin_list
