from math import exp, log
from collections import OrderedDict
from app import config


class Nuclide:
    def __init__(self, symbol, z, a, half_life):
        self.symbol = symbol
        self.z = z
        self.a = a
        self.half_life = half_life

    def decay_correction(self, ref_time, meas_time):
        dt = ref_time - meas_time
        decay_corr = exp(-log(2) * dt.total_seconds() / self.half_life[0])
        return decay_corr

    def ddm_correction(self, real_time):
        ln2 = log(2)
        hl = self.half_life[0]
        if real_time * 10000 < hl:
            return 1
        return ln2 * real_time / hl / (1 - exp(-real_time * ln2 / hl))

    def __key(self):
        return (self.a, self.z, self.half_life, self.symbol)

    def __hash__(self):
        return hash(self.__key)

    def __eq__(self, other):
        if not self.a == other.a:
            return False
        if not self.z == other.z:
            return False
        return True

    def __repr__(self):
        return ' '.join([
            str(i) for i in [
                self.symbol, self.z, self.half_life[0], " +/- ",
                self.half_life[1], " s"
            ]
        ])


def get_nuclide(filename):
    extract_nuclides()
    with open(config.TEMP_DB) as tmp_db:
        for line in tmp_db:
            ls = line.split()
            if all(s in filename for s in ls[1:3]):
                a = int(ls[2])
                symbol = ls[1]
                z = int(ls[0])
                half_life = half_life_from_db(ls[3:6])
                return Nuclide(symbol, z, a, half_life)
        return Nuclide('X', 0, 0, 0)


def half_life_from_db(hl):
    conversion_f = {
        'J': 86400,
        'A': 31556926,
        'H': 3600,
        'M': 60,
        'S': 1,
    }
    hl_conv = float(hl[0]) * conversion_f[hl[1]]
    hl_unc_conv = float(hl[2]) * conversion_f[hl[1]]
    return (hl_conv, hl_unc_conv)


def half_life_human_readable(hl):
    conversion_f = {
        'days': 86400,
        'years': 31556926,
        'hours': 3600,
        'munutes': 60,
        'seconds': 1,
    }
    hr = OrderedDict()
    for k, v in conversion_f.items():
        s = str(round(hl[0] / v, 2)) + ' +/- '\
            + str(round(hl[1] / v, 3)) + ' ' + k
        len_prim = hl[0] / v
        hr.update({len_prim: s})
    ret = None
    for k, v in sorted(hr.items()):
        ret = v
        if k > 1:
            return v
    return ret


def extract_nuclides():
    with open(config.DB_FILE, errors='ignore') as db_file,\
            open(config.TEMP_DB, 'w') as tmp_db:
        for line in db_file:
            if 'NUCL' in line:
                s = ' '.join(str(l) for l in line.split()[1:7])
                tmp_db.write(s + '\n')
