from app import config

if config.THEME == 'Dark':
    bgn = "#282c34"
    red = "#e06c75"
    green = "#98c379"
    yellow = "#e5c07b"
    blue = "#61afef"
    pink = "#c678dd"
    cyan = "#56b6c2"
    white = "#dcdfe4"

if config.THEME == 'Light':
    white = "#282c34"
    red = "#e06c75"
    green = "#98c379"
    yellow = "#e5c07b"
    blue = "#61afef"
    pink = "#c678dd"
    cyan = "#56b6c2"
    bgn = "#dcdfe4"

