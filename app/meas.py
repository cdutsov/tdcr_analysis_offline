from os import name, path
from sys import path as syspath
from math import pow, sqrt
from dataclasses import dataclass
from datetime import datetime
from numpy import genfromtxt, cov
from app import analyze, config, nuclide, runtime_config
if name == 'nt':
    syspath.append(path.join(config.BASEDIR, 'tdcr18win'))
    from tdcr18win import tdcr18
else:
    from tdcr18nix import tdcr18


class Counters:
    def __init__(self, counters):
        self.raw_counters = {k: float(v) for k, v in counters.items()}
        self.corrected_counters = self.raw_counters.copy()
        self.counters = self.raw_counters.copy()

    def __repr__(self):
        r = ''
        for k, v in self.counters.items():
            r += str(k) + ':\t' + str(v) + '\t'
        return r

    def __sub__(self, other):
        x = self.counters
        y = other.counters
        counters = {
            k: vs - vo
            for k, vs, vo in zip(x.keys(), x.values(), y.values())
        }
        return Counters(counters=counters)

    def __mul__(self, other):
        x = self.counters
        counters = {k: v * other for k, v in x.items()}
        return Counters(counters=counters)

    def __rmul__(self, other):
        x = self.counters
        counters = {k: v * other for k, v in x.items()}
        return Counters(counters=counters)

    def t_over_ab(self) -> float:
        return self.counters['T'] / self.counters['AB']

    def t_over_bc(self) -> float:
        return self.counters['T'] / self.counters['BC']

    def t_over_ac(self) -> float:
        return self.counters['T'] / self.counters['AC']

    def t_over_d(self) -> float:
        return self.counters['T'] / self.counters['D']

    def correct_accidentals(self, cw_ns):
        cw = cw_ns * 1e-9
        self.corrected_counters = self.raw_counters.copy()
        c = self.corrected_counters
        pa = c['A'] - c['AC'] - c['AB'] + c['T']
        pb = c['B'] - c['AB'] - c['BC'] + c['T']
        pc = c['C'] - c['AC'] - c['BC'] + c['T']
        pab = c['AB'] - c['T']
        pbc = c['BC'] - c['T']
        pac = c['AC'] - c['T']
        ps = pa + pb + pc
        pd = pab + pbc + pac
        pt = c['T']
        self.corrected_counters['AB'] -= (
            2 * (pa * pb + pa * pbc + pb * pac + pac * pbc) + (ps + pd - pab) *
            (pab + pt)) * cw
        self.corrected_counters['BC'] -= (
            2 * (pb * pc + pb * pac + pc * pab + pab * pac) + (ps + pd - pbc) *
            (pbc + pt)) * cw
        self.corrected_counters['AC'] -= (
            2 * (pa * pc + pa * pbc + pc * pab + pab * pbc) + (ps + pd - pac) *
            (pac + pt)) * cw
        self.corrected_counters['D'] -= (2 *
                                         (pa * pb + pb * pc + pc * pa) + ps *
                                         (pd + pt)) * cw
        self.corrected_counters['T'] -= (
            2 * (pa * pbc + pb * pac + pc * pab) + (ps + pd) * pt + 2 *
            (pbc * pab + pac * pbc + pac * pab)) * cw

    def use_acc_corrected(self):
        if not runtime_config.CORRECT_ACC:
            self.counters = self.raw_counters.copy()
        else:
            self.counters = self.corrected_counters.copy()


@dataclass
class Packet:
    counters: Counters
    dt: float = 0
    cw: float = 0
    rt: float = 0

    def __sub__(self, other):
        return Packet(self.counters - other.counters, self.dt, self.cw,
                      self.rt)

    def __mul__(self, other):
        return Packet(self.counters * other, self.dt, self.cw, self.rt)

    def __rmul__(self, other):
        return Packet(self.counters * other, self.dt, self.cw, self.rt)


@dataclass
class AvgPacket:
    counters: Counters
    cov_matrix: [[] * 8]
    dt: float = 0
    cw: float = 0
    rt: float = 0
    runs: int = 0

    def pretty_print(self):
        print_string = ''
        for key in self.counters.counters.keys():
            if key in config.COINC_NAMES:
                print_string += '{:^10}\t'.format(key)
        print_string += '\n'
        for key, value in self.counters.counters.items():
            if key in config.COINC_NAMES:
                print_string += '{:>10}\t'.format(round(value, 4))
        print_string += '\n'
        for i in range(3, 8):
            sigma = pow(self.cov_matrix[i][i] / self.runs, 1 / 2)
            print_string += '{:>10}\t'.format(round(sigma, 4))
        print_string += '\n'
        return print_string


class Run:
    def __init__(self, number, start_time, rt, packets):
        packet_names = config.PKG_NAME
        self.number = number
        self.start_time = start_time
        self.packets = {k: v for k, v in zip(packet_names, packets)}
        self.net = None
        self.net_corr = None
        self.rt = rt

    def __repr__(self):
        s = ''
        s += str(self.number) + ':\t'
        s += self.start_time.isoformat()
        for packet in self.packets.values():
            s += repr(packet)
        return s


@dataclass
class HWInfo:
    device: str = 'None'
    sn: int = 0
    fw_ver: str = 'None'
    fpga_v: str = 'None'


@dataclass
class Thresholds:
    a: float = 0
    b: float = 0
    c: float = 0


@dataclass
class MeasData:
    pkg_parameters: dict()
    nuclides: nuclide.Nuclide
    user: str()
    cocktail: str()
    hv: float
    gap: float
    # preset: float = 0
    # set_runs: int = 0
    # stop_timer: str = 'None'
    # tot_runs: int = 0
    start_time: datetime
    hw_info: HWInfo
    thresholds: Thresholds


class Measurement():
    def __init__(self, filename, meas_data, runs):
        self.filename = filename
        self.basename = path.basename(filename)
        self.meas_type = analyze.find_type(filename)
        self.meas_data = meas_data
        self.meas_data.nuclides = nuclide.get_nuclide(self.basename)
        self.bgn_guess = None
        self.runs = runs
        self.averages = None
        self.net_average = None
        self.net_corr_average = None
        self.activities = {}
        self.pmt_qes = {}
        self.fom = {}
        self.eff_d = {}
        self.eff_t = {}

    def correct_for_accidentals(self):
        for run in self.runs:
            for p_name, packet in run.packets.items():
                packet.counters.correct_accidentals(packet.cw)
                packet.counters.use_acc_corrected()
        if not self.bgn_guess:
            return
        for run in self.bgn_guess.runs:
            for p_name, packet in run.packets.items():
                packet.counters.correct_accidentals(packet.cw)
                packet.counters.use_acc_corrected()

    def calculate_average(self, packet_name, obj):
        init_run = self.runs[0].packets[packet_name]
        series = []

        # Skip first measurement
        start = 0
        if runtime_config.SKIP_FIRST:
            start = 1

        for run in self.runs[start:]:
            for k, v in obj(run):
                if k == packet_name:
                    series.append(v)
        avg = lambda x: sum(x) / len(x)
        cnts = lambda k: [p.counters.counters[k] for p in series]
        a = {k: avg(cnts(k)) for k in config.C_NAMES}
        cters = [cnts(k) for k in config.C_NAMES]
        tdcr_count = [
            p.counters.counters['T'] / p.counters.counters['D'] for p in series
        ]
        covariance = cov(cters + [tdcr_count])
        if self.bgn_guess and self.bgn_guess.averages:
            bgn_covar = self.bgn_guess.averages[packet_name].cov_matrix
            for i in range(0, 8):
                covariance[i][i] += bgn_covar[i][i]
        return AvgPacket(counters=Counters(a),
                         cov_matrix=covariance,
                         dt=init_run.dt,
                         cw=init_run.cw,
                         rt=init_run.rt,
                         runs=len(self.runs))

    def calculate_averages(self, obj):
        return {k: self.calculate_average(k, obj) for k in config.PKG_NAME}

    def update_averages(self):
        self.averages = self.calculate_averages(lambda x: x.packets.items())
        if self.bgn_guess:
            self.bgn_guess.averages = self.bgn_guess.calculate_averages(
                lambda x: x.packets.items())

    def subtract_background(self):
        self.update_averages()
        for run in self.runs:
            # print(self.basename, self.bgn_guess.basename)
            # TODO: Smart background packet determination
            run.net = {
                k: run.packets[k] - self.bgn_guess.averages[k]
                for k in config.PKG_NAME
            }

    def correct_net(self):
        ddm_corr = self.meas_data.nuclides.ddm_correction
        for run in self.runs:
            decay_corr = self.meas_data.nuclides\
                .decay_correction(meas_time=run.start_time,
                                  ref_time=runtime_config.REFERENCE_DATE)
            run.net_corr = {
                k: run.net[k] * decay_corr * ddm_corr(run.rt)
                for k in config.PKG_NAME
            }

    def calculate_net_average(self):
        self.net_average = self.calculate_averages(lambda x: x.net.items())
        # print(self.net_average['N1'])

    def calculate_net_corr_average(self):
        self.correct_net()
        self.net_corr_average = self.calculate_averages(
            lambda x: x.net_corr.items())

    def has_activity(self):
        available_nuclides = [
            nuclide.get_nuclide(n) for n in config.AVAILABLE_NUCLIDES
        ]
        if any([
                self.meas_data.nuclides == av_nucl
                for av_nucl in available_nuclides
        ]):
            return True
        return False

    def calculate_activity(self):
        iz = self.meas_data.nuclides.z
        ia = self.meas_data.nuclides.a
        tanxia_file = config.STOPPING_POWER_DB
        tx_data = genfromtxt(tanxia_file, delimiter='\t')
        aee = tx_data[:, 0]
        adedx = tx_data[:, 1:]
        kbs = runtime_config.KBS
        c = runtime_config.COCKTAIL_NUMBER
        pkg = runtime_config.PACKET_NAME
        tsurab = self.net_corr_average[pkg].counters.t_over_ab()
        tsurbc = self.net_corr_average[pkg].counters.t_over_bc()
        tsurac = self.net_corr_average[pkg].counters.t_over_ac()
        for kb in kbs:
            eqa, eqb, eqc, fom, eff_d, eff_t = \
                tdcr18.tdcr18(
                    iz=iz, ia=ia, aee=aee, adedx=adedx, akb=kb, tsurab=tsurab,
                    tsurbc=tsurbc, tsurac=tsurac, ic=c)
            self.fom.update({kb: fom})
            self.eff_d.update({kb: eff_d})
            self.eff_t.update({kb: eff_t})
            self.pmt_qes.update({kb: (eqa, eqb, eqc)})
            activity = self.net_corr_average[pkg]\
                .counters.counters['D'] / eff_d
            uncertainty = self.calculate_uncertainty(pkg, activity)
            delta = uncertainty / activity * 100
            self.activities.update({kb: (activity, uncertainty, delta)})

    def calculate_uncertainty(self, pkg, activity):
        td = self.net_corr_average[pkg].counters.t_over_d()
        var_td = self.net_corr_average[pkg].cov_matrix[8][8]

        # Get the double coincidences variance
        var_d = [
            self.net_corr_average[pkg].cov_matrix[i][i] for i in range(3, 8)
        ]
        # Get the covariance of AB with TDCR and so on...
        cov_d_td = [
            self.net_corr_average[pkg].cov_matrix[i][8] for i in range(3, 8)
        ]
        # Get AB, BC, AC, D counting rates
        d = [
            self.net_corr_average[pkg].counters.counters[c]
            for c in config.COINC_NAMES
        ]

        # Calculate variance of AB/TDCR, BC/TDCR and AC/TDCR
        var = 0
        for i in range(0, 3):
            var += (var_d[i] / d[i]**2 + var_td / td**2 -
                    2 * cov_d_td[i] / d[i] / td) * d[i]**2 / td**2

        # Add the variance of -2T/TDCR which is var(2D)
        var += 4 * var_d[3]
        sigma = sqrt(var / self.net_corr_average[pkg].runs)
        delta = sigma / (d[3] / td)
        uncertainty = delta * activity
        return uncertainty
