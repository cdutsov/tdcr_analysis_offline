from difflib import SequenceMatcher
from collections import OrderedDict
from app import config, io, runtime_config, nuclide


def summarize(all_meas):
    filenames = []
    nuclides = []
    for m in all_meas:
        filenames.append(m.basename)
        nuclides.append(m.meas_data.nuclides)

    filenames = list(set(filenames))
    nuclides = list(set(nuclides))
    return filenames, nuclides


def print_summary(filenames, nuclides):
    print('-------------')
    print("Found {0} files with filenames:".format(len(filenames)))
    for f in filenames:
        print(f)
    print("Files contain the following nuclides:")
    for n in nuclides:
        print(n.symbol, '-', n.z, end=',\t')
    print('\n-------------')


def find_background(meas_name, all_meas):
    # filenames, nuclides = summarize(all_meas)
    candidates = OrderedDict()
    best_candidate = (None, 0.0)
    for m in all_meas:
        if any(b for b in config.BACKGROUND_NAMES if b in m.basename):
            d = SequenceMatcher(None, m.basename, meas_name)
            candidates.update({m: d.ratio()})
    for m, d in candidates.items():
        if best_candidate[1] < d:
            best_candidate = (m, d)
    return best_candidate


def find_type(meas_name):
    if any(b for b in config.BACKGROUND_NAMES if b in meas_name):
        return config.BGN
    return config.SRC


def guess_backgrounds(all_meas):
    i = 0
    meas_with_bgn = []
    print("Guessing background measurements...")
    for meas in [a for a in all_meas if not a.meas_type == config.BGN]:
        i += 1
        guess = find_background(meas.basename, all_meas)
        bgn_guess, d = guess
        if d > 0.2:
            meas.bgn_guess = bgn_guess
            meas_with_bgn.append(meas)
            print('{3}: Background {0}\
                  will be used for measurement {1} with certainty:\
                  {2:8.3f}'.format(meas.bgn_guess.basename, meas.basename, d,
                                   i))
        else:
            print(config.bcolors.FAIL +
                  '{0}: Measurement {1} has no valid background candidate!!!'.
                  format(i, meas.basename) + config.bcolors.ENDC)
    return meas_with_bgn


def subtract_backgrounds(all_meas):
    print("Subtracting backgrounds from measurements...")
    # for meas in [a for a in all_meas if not a.meas_type == config.BGN]:
    for meas in [a for a in all_meas]:
        meas.subtract_background()


def calculate_net_averages(all_meas):
    print("Calculating net averages...")
    for meas in [a for a in all_meas if not a.meas_type == config.BGN]:
        meas.calculate_net_average()


def calculate_net_corr_averages(all_meas):
    print("Calculating decay corrected net averages...")
    for meas in [a for a in all_meas if not a.meas_type == config.BGN]:
        meas.calculate_net_corr_average()


def calculate_activities(all_meas):
    print("Running TDCR18 code...")
    for meas in [a for a in all_meas if not a.meas_type == config.BGN]:
        if meas.has_activity():
            meas.calculate_activity()


def auto_analyze():
    all_meas = io.import_files_from_folder(runtime_config.WORK_DIR)
    # f, n = summarize(all_meas)
    # print_summary(f, n)
    meas_with_bgn = guess_backgrounds(all_meas)
    return meas_with_bgn


def calculate_all(meas_with_bgn):
    for meas in meas_with_bgn:
        meas.correct_for_accidentals()
    subtract_backgrounds(meas_with_bgn)
    calculate_net_averages(meas_with_bgn)
    calculate_net_corr_averages(meas_with_bgn)
    calculate_activities(meas_with_bgn)
    for meas in meas_with_bgn:
        # print(meas.print_final_result())
        yield io.print_final_result(meas)
    yield io.print_kb_summary(meas_with_bgn)
    yield ''.join(['\nProgram Version: ', config.PROGRAM_VERSION])
