from os import path, listdir
from datetime import timedelta, datetime
from app import meas, runtime_config, config, nuclide
from .meas import Packet, Counters, Run
from xml.etree.ElementTree import ElementTree, fromstring
from collections import defaultdict


# Helper funcitons
def parse_line(line):
    return line.split(',')[1].strip()


def fparse_lines(lines):
    for line in lines:
        yield float(parse_line(line))


def etree_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(etree_to_dict, children):
            for k, v in dc.items():
                dd[k].append(v)
        d = {t.tag: {k: v[0] if len(v) == 1 else v for k, v in dd.items()}}
    if t.attrib:
        d[t.tag].update(('@' + k, v) for k, v in t.attrib.items())
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
                d[t.tag]['#text'] = text
        else:
            d[t.tag] = text
    return d


class InputParameters():
    def __init__(self):
        self.kb = 0.0


class XMLParser():
    def __init__(self, filename):
        with open(filename, encoding='iso-8859-1') as f:
            filedata = f.read()
        filedata = filedata.replace("&", "&amp;")
        tree = ElementTree(fromstring(filedata))
        xml_dict = etree_to_dict(tree.getroot())['nanoTDCR_Measurement']
        meas_start = xml_dict['measurement_start']
        hardware = xml_dict['hardware']
        settings = xml_dict['settings']
        run_lines = xml_dict['data']

        self.filename = filename
        self.meas_data = self.fill_data(meas_start, hardware, settings)
        self.runs = self.parse_runs(run_lines)

    def parse(self):
        return meas.Measurement(self.filename, self.meas_data, self.runs)

    def parse_runs(self, run_lines):
        runs = []
        for run_num, run_data in run_lines.items():
            if not float(run_data['real_time']) == 0:
                runs.append(self.parse_run(run_data))
        return runs

    def parse_run(self, run_data):
        packets = []
        start_time = self.get_date_csv(run_data['start'])
        coincidences = run_data['coincidence']['rate']['dead_time_extension']
        list_coinc = [
            dict(
                run_data['single_channel']['rate']['dead_time_extension']
                ['E1'], **coincidences['E1']['coincidence_window']['N']),
            dict(
                run_data['single_channel']['rate']['dead_time_extension']
                ['E1'], **coincidences['E1']['coincidence_window']['M']),
            dict(
                run_data['single_channel']['rate']['dead_time_extension']
                ['E2'], **coincidences['E2']['coincidence_window']['N']),
            dict(
                run_data['single_channel']['rate']['dead_time_extension']
                ['E2'], **coincidences['E2']['coincidence_window']['M']),
        ]
        rt = float(run_data['real_time'])
        for pkg, counters in zip(config.PKG_NAME, list_coinc):
            packets.append(
                Packet(cw=self.meas_data.pkg_parameters[pkg][1],
                       dt=self.meas_data.pkg_parameters[pkg][0],
                       rt=rt,
                       counters=Counters(counters)))
        return Run(number=0, start_time=start_time, rt=rt, packets=packets)

    def get_date_csv(self, meas_time):
        date = meas_time['date']
        time = meas_time['time']
        dt = date + time.strip()
        try:
            strt = '%d/%m/%Y%H:%M:%S.%f'
            return datetime.strptime(dt, strt)
        except ValueError:
            print('Trying with old datetime format')
            try:
                strt = '%d/%m/%Y%H-%M-%S.%f'
                return datetime.strptime(dt, strt)
            except ValueError:
                print('Unknown datetime format in xml file!')

    def get_hardware(self, hardware):
        hw_info = meas.HWInfo()
        hw_info.device = hardware['device']
        hw_info.sn = hardware['serial_number']
        hw_info.fw_ver = hardware['firmware']
        hw_info.fpga_v = hardware['fpga']
        return hw_info

    def get_operator(self, settings):
        return settings['operator']

    def get_cocktail(self, settings):
        return settings['coctail']

    def get_hv(self, settings):
        return 999

    def get_thresholds(self, settings):
        thresholds = meas.Thresholds()
        thresholds.a = settings['threshold']['A']
        thresholds.b = settings['threshold']['B']
        thresholds.c = settings['threshold']['C']
        return thresholds

    def get_gap(self, settings):
        gap = settings['acquisition']['time_gap']
        return gap

    def get_nuclide(self, settings):
        return settings['nuclides']

    def get_dt_cw(self, settings):
        params = [0, 0, 0, 0]
        params[0] = float(settings['dead_time_extension']['E1'])
        params[1] = float(settings['dead_time_extension']['E2'])
        params[2] = float(settings['coincidence_window']['N'])
        params[3] = float(settings['coincidence_window']['M'])
        pkg_parameters = {}
        pkg_parameters.update({'N1': (params[0], params[2])})
        pkg_parameters.update({'M1': (params[0], params[3])})
        pkg_parameters.update({'N2': (params[1], params[2])})
        pkg_parameters.update({'M2': (params[1], params[3])})
        return pkg_parameters

    def fill_data(self, meas_time, hardware, settings):
        date = self.get_date_csv(meas_time)
        hardware = self.get_hardware(hardware)
        operator = self.get_operator(settings)
        nuclide = self.get_nuclide(settings)
        cocktail = self.get_cocktail(settings)
        thresholds = self.get_thresholds(settings)
        gap = self.get_gap(settings)
        pkg_parameters = self.get_dt_cw(settings)
        return meas.MeasData(start_time=date,
                             user=operator,
                             nuclides=nuclide,
                             cocktail=cocktail,
                             thresholds=thresholds,
                             gap=gap,
                             hv=0,
                             hw_info=hardware,
                             pkg_parameters=pkg_parameters)


def print_final_result(m):
    print_lst = []
    pkg = runtime_config.PACKET_NAME
    kbs = runtime_config.KBS
    cocktail = runtime_config.COCKTAIL_NAME
    has_activity = m.has_activity()
    ref_date = runtime_config.REFERENCE_DATE
    print_lst.append(['Folder:\t', path.dirname(m.filename), '\n'])
    print_lst.append(['Filename:\t', m.basename, '\n'])
    print_lst.append(['Blank:\t', m.bgn_guess.basename, '\n'])
    nuclide_data = '{0} {1}-{2}\tHL=\t{3}'.format(
        m.meas_data.nuclides.z, m.meas_data.nuclides.symbol,
        m.meas_data.nuclides.a,
        nuclide.half_life_human_readable(m.meas_data.nuclides.half_life))
    print_lst.append(['Nuclide:\t', nuclide_data, '\n'])
    print_lst.append(['Cocktail:\t', cocktail, '\n'])
    print_lst.append(
        ['Reference time:\t',
         ref_date.strftime('%Y-%m-%d %H:%M:%S.%f')])
    print_lst.append(['\n'])
    print_lst.append([
        'Coincidence window:\t',
        str(m.meas_data.pkg_parameters[pkg][1]), ' ns\t', 'Dead time:\t',
        str(m.meas_data.pkg_parameters[pkg][0]), ' us\n\n'
    ])
    print_lst.append(
        ['Blank average counting rates and std. uncertainty of average:'])
    print_lst.append(['\n'])
    print_lst.append([m.bgn_guess.averages[pkg].pretty_print()])
    print_lst.append(['\n'])
    print_lst.append(
        ['Source net average counting rates and std. uncertainty of average:'])
    print_lst.append(['\n'])
    print_lst.append(m.net_corr_average[pkg].pretty_print())
    print_lst.append(['\n'])
    print_lst.append(['\n'])
    if not has_activity:
        print_lst.append(
            ['-------------------------------------------------\n\n'])
        return ''.join([i for sublst in print_lst for i in sublst])
    print_lst.append([
        'Qeff: A\t{0:8.4f}\tB\t{1:8.4f}\tC\t{2:8.4f}'.format(
            *m.pmt_qes[kbs[0]]), '\t'
    ])
    print_lst.append(['\n'])
    print_lst.append(['{0:<10}\t'.format('kB_val')])
    print_lst.append([
        '{0:<10}\t{1:<10}\t{2:<10}\t{3:>10}\t{4:>10}\t{5:>10}'.format(
            'FOM', 'D_eff', 'T_eff', 'Activity', 'Std. unc', 'Delta')
    ])
    print_lst.append(['\n'])
    for kb in kbs:
        print_lst.append(['{0:<10.4f}\t'.format(kb)])
        print_lst.append([
            '{0:<10.4f}\t{1:<10.4f}\t{2:<10.4f}\t'.format(
                m.fom[kb], m.eff_d[kb], m.eff_t[kb])
        ])
        print_lst.append(['{0:10.4f}\t'.format(m.activities[kb][0])])
        print_lst.append(['{0:10.4f}\t'.format(m.activities[kb][1])])
        print_lst.append(['{0:9.2f} %\t'.format(m.activities[kb][2])])
        print_lst.append(['\n'])
    print_lst.append(['-------------------------------------------------\n\n'])
    return ''.join([i for sublst in print_lst for i in sublst])


def print_kb_summary(measurements):
    kbs = runtime_config.KBS
    pkg = runtime_config.PACKET_NAME
    print_lst = ['Summary of A vs TDCR for different kBs']
    print_lst.append(['\n'])
    print_lst.append(['{0:<10}'.format('TDCR/KB')])
    print_lst.append(['\t{0:<16.4f}\t'.format(kb) for kb in kbs])
    print_lst.append(['\n'])
    for m in measurements:
        if m.has_activity():
            print_lst.append([
                '{0:<10.5f}\t'.format(
                    m.net_corr_average[pkg].counters.t_over_d())
            ])
            print_lst.append([
                '{0:<8.2f}\t{1:<8.2f}\t'.format(m.activities[kb][0],
                                                m.activities[kb][1])
                for kb in kbs
            ])
            print_lst.append(['\n'])
    return ''.join([i for sublst in print_lst for i in sublst])


def import_files_from_folder(folder):
    if path.exists(folder):
        all_meas = []
        for filename in listdir(folder):
            if filename.endswith('.xml'):
                m = import_file(path.join(folder, filename))
                all_meas.append(m)
        return all_meas
    print('DEBUG: Folder does not exist')
    return None


def import_file(filename):
    return XMLParser(filename).parse()
