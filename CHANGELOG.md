# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.3] - 2021-12-15

### Changed

- Minor fix: Reference date field in GUI now accepts three formats:
             YYYY-MM-DD HH:MM, YYYY-MM-DD HH:MM:SS and YYYY-MM-DD

## [1.1.2] - 2021-10-13

### Changed

- Minor fix: lowercase 'blanc' and 'blank' in the file name are correctly registered as a blank sample
- Added Manganese 53 half-life (from https://en.wikipedia.org/wiki/Isotopes_of_manganese#Manganese-53)

## [1.1.1] - 2020-12-19

### Changed

- Minor fix: when reading xml files the windows and dead times were switched
- Cocktail name is now in output
- Removed some useless debug messages

## [1.1.0] - 2020-12-14

### Changed

- Added option for calculation of accidental coincidences
- Using csv files no longer works - please use xml files
- Due to the fact that the xml files are non-standard please do not save
  files containing ampersand, greather than, less than or other special symbols.
  It is beyond my power to fix that and it should be fixed in the nanoTDCR software.

## [1.0.1] - 2020-01-05

### Changed

- Minor change in versioning printing
- Fix of uncertainty calculation

## [1.0.0] - 2020-01-05

### Added

- Calculation of uncertainty of the activity
- Started versioning of the software
- Output now shows the version of the software used and the TDCR calc. program
- Removed TDCR18 binaries from git
