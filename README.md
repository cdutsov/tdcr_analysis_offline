### TDCR offline analysis program
## Introduction
This is a software designed to analyze large amounts of nanoTDCR measurements.
It can use the TDCR18 Fortran code by Philippe Cassette, PhD, to calculate the
activity of a number of pure-beta emitters.

![Screenshot](./screenshot_1.png)
Screenshot from the program.

## Installation
In order to install the program simply download this repository and run:
```bash
python run.py
```
To use the TDCR18 code you would need to compile it with `f2py`. Get the TDCR18 source
code and compile all files. If on Windows, please use Python version 3.7. It is possible
that the code works also with newer versions, but it is not tested. 

Please be sure to compile the Fortran code with `f2py` from the same Python version and on the 
same computer.

In case of problems, please leave an Issue in this repository.
